// Доповнити функцію createNewUser() з попередньої домашньої роботи 
// методами підрахунку віку користувача та його паролем.

// Технічні вимоги:
// Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
// + При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
// + Створити метод getAge() який повертатиме скільки користувачеві років.
// + Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, 
// з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
// + Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.

// Варіант з крапкою в форматі дати
function createNewUser() {

  const newUser = {};

  newUser.firstName = prompt("Enter your name.");
  newUser.lastName = prompt("Enter your surname.");
  newUser.getLogin = function () { return "Your Login: " + this.firstName[0].toLowerCase() + this.lastName.toLowerCase() }; // формуємо логін

  let birthdayAsk = prompt("Enter your birthday in the following format (dd.mm.yyyy), use dots . ", "dd.mm.yyyy");
  let day, month, year; //оголошуємо частини дати поза межами наступної функції, щоб не порушувати зони видимості

  while (birthdayAsk.length !== 10 || birthdayAsk[2] !== `.` || birthdayAsk[5] !== `.`) {
    birthdayAsk = prompt("Enter your birthday in the correct format (dd.mm.yyyy), field allows 8 digits and two semicolons", "dd.mm.yyyy");
  }

  day = birthdayAsk.slice(0, 2);
  month = birthdayAsk.slice(3, 5) - 1; //місяці рахуються з 0, тому мінус 1
  year = birthdayAsk.slice(6);

  // console.log("You were born:" + day + "." + (month + 1) + "." + year);   

  let birthday = new Date(year, month, day); // збираємо дату народження
  newUser.birthday = birthday;    //додаємо дату народження в елементи об'єкта

  let today = new Date();  // Поточна дата на даний момент

  if (today.getMonth() < newUser.birthday.getMonth() || 
  (today.getMonth() === newUser.birthday.getMonth() && today.getDate() < newUser.birthday.getDate())
  ) {newUser.getAge = function() {return "Your age: " + (today.getFullYear() - newUser.birthday.getFullYear() - 1)}
  }  else {newUser.getAge = function() {return "Your age: " + (today.getFullYear() - newUser.birthday.getFullYear())}; };  // Робимо перевірку, можливо виповнилася вже дата народження в цьому році чи ще ні
  console.log(newUser.getAge());  //вираховуємо вік в роках

  newUser.getPassword = function () {
    return "Your password: " + this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear()
  };  // формуємо пароль

  return newUser;    //повертаємо увесь об'єкт
};

let newUser1 = createNewUser();
console.log(newUser1);
console.log(newUser1.getLogin());
console.log(newUser1.getAge());
console.log(newUser1.getPassword());
