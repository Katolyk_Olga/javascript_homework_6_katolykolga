// 1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
// Багато символім в мові програмування можуть виконувати службову роль, наприклад всі математичні оператори,
// вони безпосередньо входять до синтаксису javascript. Тому якщо нам потрібно, щоб программа проігнорувала символ, 
// щоб вона прийняла його як частинку текстового контенту, нам потрібно якось це обозначити, 
// екранувати за допомогою зворотнього слеша \
// 
// 2. Які засоби оголошення функцій ви знаєте?
// - function declaration (function (параметри) {тіло})
// - funxtion expresion (оголошується function () {} при присвоєнні = змінній, 
//  в такому випадку назва для функції не обов'язкова, назвою стає змінна)
// - стрілочні функції (починаються одразу з умови в круглих дужках, далі => {})
// - в об'єкті як елемент об'єкта функція виступає у ролі властивості-метода, 
// його ми вставляємо як властивість об'єкта: назва() і присвоюємо тіло як значення властивості.
// 
// 3. Що таке hoisting, як він працює для змінних та функцій?
// Hoisting - це про область видимості змінної типу var та функції типу function declaration. 
// Це механізм, коли змінну var або функцію видно раніше оголошення,
// тобто їхня дія пересувається вгору своєї області видимості перед тим, як код буде виконаний. 
// Піднімаються змінні типу var та function declaration по різному: var буде зі значенням undefined, а функція виконається.
//
//

// Доповнити функцію createNewUser() з попередньої домашньої роботи 
// методами підрахунку віку користувача та його паролем.

// Технічні вимоги:
// Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
// + При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
// + Створити метод getAge() який повертатиме скільки користувачеві років.
// + Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, 
// з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
// + Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.


function createNewUser() {

  const newUser = {};    

  newUser.firstName = prompt("Enter your name.");   
  newUser.lastName = prompt("Enter your surname.");      
  newUser.getLogin = function() {return "Your Login: " + this.firstName[0].toLowerCase() + this.lastName.toLowerCase()}; // формуємо логін

  let birthdayAsk = prompt("Enter your birthday in a certain format as 8 digits (ddmmyyyy):", "ddmmyyyy");
  let day, month, year; //оголошуємо частини дати поза межами наступної функції, щоб не порушувати зони видимості

  while (birthdayAsk.length !== 8) {
    birthdayAsk = prompt("Field allows 8 digits. Enter your birthday in a correct format without any symbols (ddmmyyyy):", "ddmmyyyy");
  }
    
  day = birthdayAsk.slice(0, 2);     //розкладаємо рядок на частини для формування дати народження
  month = birthdayAsk.slice(2, 4) - 1; //місяці рахуються з 0, тому мінус 1
  year = birthdayAsk.slice(4);
  
  // console.log("You were born:" + day + "." + (month + 1) + "." + year);   
  // тут в місяці +1, щоб відмінити мінус 1 вище в рядку про місяць, бо ми просто хочемо в консоль передати значення, яке ввів користувач.

  let birthday = new Date (year, month, day); // збираємо дату народження
  newUser.birthday = birthday;    //додаємо дату народження в елементи об'єкта

  let today = new Date();  // Поточна дата на даний момент
    
  if (today.getMonth() < newUser.birthday.getMonth() || 
  (today.getMonth() === newUser.birthday.getMonth() && today.getDate() < newUser.birthday.getDate())
  ) {newUser.getAge = function() {return "Your age: " + (today.getFullYear() - newUser.birthday.getFullYear() - 1)}
  }  else {newUser.getAge = function() {return "Your age: " + (today.getFullYear() - newUser.birthday.getFullYear())}; };  // Робимо перевірку, можливо виповнилася вже дата народження в цьому році чи ще ні
  console.log(newUser.getAge());  //вираховуємо вік в роках

  newUser.getPassword = function() {
    return "Your password: " + this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear()
  };   // формуємо пароль
  
  return newUser;    //повертаємо увесь об'єкт
};

let newUser1 = createNewUser();
console.log(newUser1);
console.log(newUser1.getLogin());;
console.log(newUser1.getAge());
console.log(newUser1.getPassword());;




// Варіант з крапкою і split

// function getBirthday() {
//     let birthdayAsk = prompt("Введіть дату народження (dd.mm.yyyy):");
//     // Розбиваємо введений текст на компоненти дати
//     let birthdayElements = birthdayAsk.split('.');
    
//     if (birthdayElements.length === 3) {   // Перевіряємо коректність ввода дати, причому split розбив на 3 елементи масиву!
//       let day = parseInt(birthdayElements[0]);
//       let month = parseInt(birthdayElements[1]) - 1;
//       let year = parseInt(birthdayElements[2]);
  
//       let birthday = new Date(year, month, day); // Збираємо дату
//     }
//   }




//       alert("Incorrect date format. Please enter in the format dd.mm.yyyy.");



